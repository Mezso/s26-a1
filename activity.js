const http = require('http');

const PORT = 3000;

http.createServer(function(request, response){

	if(request.url == '/login'){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('Welcome to the login page')
	}
	else{
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found")

	}
}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)

/*
1. What directive is used by Node.js in loading the modules it needs?
Answer : import directive

2. What Node.js module contains a method for server creation?
Answer : http module

3. What is the method of the http object responsible for creating a server using Node.js
Answer : . createServer() method

4. What method of the response object allows us to set status codes and content types?
Answer : response.writeHead(200,{'Content-Type': 'text/plain'})


5. Where will console.log() output its content when run in Node.js?
Answer : at the CMD

6. What property of the request object contains the address's endpoint?
Answer : query string, parameters, body

*/